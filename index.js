const path = require('path')
const fs = require('fs')
const mime = require('mime')
const os = require('os')
const http = require('http')

const Koa = require('koa')
const KoaRouter = require('koa-router')
const BodyParser = require('koa-bodyparser')
const cors = require('@koa/cors')

const ArtefactStore = require('artefact-store')
const log = require('debug')('artefact-server')

const busboy = require('async-busboy')

module.exports = async function ArtefactServer (options = {}) {
  const {
    host = 'localhost',
    port = 1234,
    pataka = false,
    storage = options.dev
      ? path.join(os.tmpdir(), `artefact-store-${Date.now()}`)
      : path.join(os.homedir(), '.artefact-store'),
    storeOpts = {}
  } = options

  log(`starting artefact-store ${pataka ? '(pataka mode)' : '(normal mode)'}`)

  const store = new ArtefactStore(storage, { ...storeOpts, pataka })
  await store.ready()
  log('store.storagePath: ' + storage)
  if (!pataka) log('store.driveAddress: ' + store.driveAddress.toString('hex'))

  const app = new Koa()
  const router = new KoaRouter()
  app.use(BodyParser())
  app.use(async (ctx, next) => {
    log(`${ctx.method} ${ctx.url} ${JSON.stringify(ctx.request.body)}`)
    await next()
  })
  app.use(cors())
  app.use(router.routes())
  // .use(router.allowedMethods())

  // TODO - consider adding control for whether POST /drive /blob are available
  router.post('/drive', async (ctx) => {
    const ok = await store.addDrive(ctx.request.body.driveAddress)
      .catch((err) => {
        console.error(err)
        return { error: err.message }
      })
    ctx.status = ok.error ? 422 : 200
    ctx.body = ok.error ? ok : { ok: true }
  })

  const addFileToStore = AddFileToStore(store, host, port)

  if (!pataka) {
    router.post('/blob', async (ctx) => {
      // handle request because of distinct behavior for desktop and mobile
      const { fileStream, fileName, readKey } = await readBlobRequest(ctx)
      const result = await addFileToStore(fileStream, fileName, readKey)
        .catch(err => ({ error: err.message }))

      ctx.status = result.error ? 422 : 200
      ctx.body = result
    })

    router.get('/drive/:driveAddress/blob/:blobId', async (ctx) => {
      const blobId = ctx.params.blobId
      const driveAddress = Buffer.from(ctx.params.driveAddress, 'hex')

      let { readKey, mimeType, fileName } = ctx.query
      if (!readKey) {
        ctx.status = 400
        return
      }
      readKey = Buffer.from(readKey, 'hex')
      const type = (
        mimeType ||
        mime.getType(path.extname(blobId) || fileName)
      )
      if (type) ctx.type = type

      let [start, end] = determineRange(ctx)
      const fileSize = await store.fileSize(blobId, driveAddress)

      if (!start && !end) {
        ctx.set({
          'Content-Type': type,
          'Accept-Ranges': 'bytes',
          'Content-Length': fileSize
        })
        ctx.status = 200
        ctx.body = await store.getReadStream(blobId, driveAddress, readKey)
        return
      }

      end = end || fileSize - 1 // NOTE -1 because filesize counts from 1 but start counts from 0?
      ctx.set({
        'Content-Type': type,
        'Accept-Ranges': 'bytes',
        'Content-Length': end - start + 1,
        'Content-Range': `bytes ${start}-${end}/${fileSize}`
      })
      ctx.status = 206
      ctx.body = await store.getReadStream(blobId, driveAddress, readKey, { start, end })
    })
  }

  const server = http.createServer(app.callback())

  return new Promise((resolve, reject) => {
    server.listen(port, host, (err) => {
      if (err) return reject(err)

      log(`listening on http://${host}:${port}`)

      resolve({
        host,
        port,
        pataka,
        store,
        close () {
          return Promise.all([
            new Promise((resolve, reject) => {
              server.close(err => err ? reject(err) : resolve())
            }),

            store.close() // promise
          ])
            .then(errs => errs.every(err => err == null) ? null : errs)
        }
      })
    })
  })
}

function determineRange (ctx) {
  if (ctx.headers.range) {
    // unit=start-end   (end optional!, note can be multiple of these)
    const [unit, rangeBit] = ctx.headers.range.split('=') // eslint-disable-line

    return rangeBit
      .split('-')
      .map(str => str.length ? parseInt(str) : undefined)
  }

  // TODO consider disabling this
  return [parseInt(ctx.query.start), parseInt(ctx.query.end)]
}

async function readBlobRequest (ctx) {
  log('reading blob request')
  // check if the request is multipart/form-data to handle requests in a distinct way
  const isMultipart = ctx.is('multipart')

  if (!isMultipart) {
    // handle json
    const filePath = ctx.request.body.localFilePath

    return {
      fileName: ctx.request.body.blobId || path.basename(filePath),
      readKey: ctx.request.body.readKey,
      fileStream: fs.createReadStream(filePath)
    }
  }

  // handle multipart with file and fields
  log('running busboy for multipart request')
  const { files, fields } = await busboy(ctx.req)
  log('ran busboy')
  const fileStream = files[0]

  return {
    fileName: fields.blobId || fileStream.filename,
    readKey: fields.readKey,
    fileStream
  }
}

function AddFileToStore (store, host, port) {
  return async function addFileToStore (fileStream, fileName, fileEncryptionKey) {
    log('adding file to store with store.addFileStream')
    const result = await store.addFileStream(fileStream, fileName, {
      optionalFileEncryptionKey: fileEncryptionKey && Buffer.from(fileEncryptionKey, 'hex')
    })
    log('added file to store')

    const url = new URL(`http://${host}:${port}/drive/${result.driveAddress}/blob/${result.fileName}`)
    url.search = new URLSearchParams({
      readKey: result.fileEncryptionKey,
      fileName
    })

    return {
      driveAddress: result.driveAddress,
      blobId: result.fileName,
      readKey: result.fileEncryptionKey,
      fileName,
      href: url.href
    }
  }
}
