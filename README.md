# Artefact Server

## Usage Example

```js
const ArtefactServer = require('artefact-server')

ArtefactServer({ storage: '/tmp/artefact-example' })
  .then(server => {
    console.log('artefact server running!')
    console.log(server)
    //

    server.close()
      .then(() => console.log('shutdown!')
  })
```

## API

This server can be started in modes: normal/pataka

In pataka mode, the server will do full replication of drives it adds, and
will not offer http based file posting or serving.
(only http based registration of drives to replicate)

### `ArtefactServer(opts) => Promise`

Starts a local drive (if not in pataka mode), and spins up a web server.

- `opts` *Object* optional startup configuration with properties:
    - `pataka` *Boolean* whether this is starting in pataka mode (default `false`)
    - `dev` *Boolean* whether to start in dev mode. This auto-sets `opts.storage` to a tmp folder (default :`false`)
    - `host` *String* host for the file server (default: `'localhost'`)
    - `port` *Number* the port for the file server (default: `1234`)
    - `storage` *String* absolute path to where you want to store blobs (default: `~/.artefact-store`)
    - `storeOpts` *Object* options you want internal artefact-store to be started with. (see [artefact-store](https://gitlab.com/ahau/artefact-store) docs)

- `Promise` return resolves with a an object:
    ```
    {
      pataka, // boolean
      host,
      port,
      store,  // artefact-store instance (inspect for more detail)
      close   // function which close server and internal store. returns Promise
    }
    ```


### POST `/drive`

connect to a driveAddress, and if in pataka mode, fully replicate the drive

Body:
- `driveAddress` - the address of a remote drive (32 bytes, hex)

responds with:
```json
{ "ok": "true" }
```
or
```json
{ "error": "error message" }
```

### POST `/blob`

add a file to your drive (not available in pataka mode)

Body:
- `localFilePath`: the path to a local file to add
- `readKey`: key to use for encrypting the file (32 bytes, hex) - optional.  If not given, will create one.
- `blobId`: the id/ path to give the file on the drive - optional. If not given, will be derived from `localFilePath`

responds with:
```json
{
  "driveAddress": "...",
  "readKey": "...",
  "blobId": "...",
  "fileName": "..." // original fileName
}
```

### GET `/drive/:driveAddress/blob/:blobId`

retrieve a file (not available in pataka mode)


Params:
- `driveAddress` - address of drive where blob is stored (hex)
- `blobId` - id of blob within that store

Query:
- `readKey` *String* (hex)
    - decryption key which allows you to read the content of the blob
    - **required** (may not be required in future to allow pulling the encrypted blob)
- `start` *Number* (optional)
    - byte offset to begin stream
    - default: `0`
- `end` *Number* (optional)
    - byte offset to stop stream
    - default: `EOF`
- `fileName` *String* (optional)
    - if provided, this value will be used to derive a mime type on the response

responds with a binary stream (for use with [`render-media`](https://github.com/feross/render-media)) - can also be viewed directly in the browser (but seeking not available)


### coming soon: GET `/` - send front-end to browser (not yet merged)


## CLI

to install, npm link [artefact-store](https://gitlab.com/ahau/artefact-store) and [artefact-render](https://gitlab.com/coboxcoop/try-render-media)
to run:
`./bin.js <options>`

Options can be:

- `--help` display usage information
- `--host <hostname>` set hostname (defaults to localhost)
- `--port <port>` set port number (defaults to 1234)
- `--storage <path>` set local storage directory (defaults to ~/.artefact-store)
- `--pataka` start in pataka mode

