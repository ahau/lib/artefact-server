const test = require('tape')
const path = require('path')
const fs = require('fs')
const axios = require('axios')
const FormData = require('form-data')
const ArtefactServer = require('../')
const encryption = require('artefact-store/encrypt')

test('Blob-Upload', async t => {
  const server = await ArtefactServer({ dev: true })
  const driveAddress = server.store.driveAddress.toString('hex')
  const filePath = path.join(__dirname, '../README.md')
  const fileName = path.basename(filePath)
  const url = new URL(`http://${server.host}:${server.port}/blob`)

  await uploadJson()
  await uploadFormData()

  async function uploadJson () {
    t.comment('json request body')

    const readKey = encryption.key().toString('hex')
    const res = await axios(url.href, {
      method: 'post',
      data: {
        localFilePath: filePath,
        readKey
      }
    })

    t.equal(res.status, 200, 'http statusCode 200')
    t.notEqual(res.data, null, 'http body not null')
    t.notEqual(res.data, undefined, 'http body not undefined')
    t.equal(res.data.driveAddress, driveAddress, `driveAddress is ${driveAddress}`)
    t.equal(res.data.blobId, fileName, `blobId is ${fileName}`)
    t.equal(res.data.readKey, readKey, `readkey is ${readKey}`)
    t.equal(res.data.fileName, fileName, `file name is ${fileName}`)

    const expectedUrl = `http://${server.host}:${server.port}/drive/${driveAddress}/blob/${fileName}?readKey=${readKey}&fileName=${fileName}`
    t.equal(res.data.href, expectedUrl, `href is ${expectedUrl}`)
  }

  async function uploadFormData () {
    t.comment('multipart/form-data request body')

    const readKey = encryption.key().toString('hex')
    const formData = new FormData()
    formData.append('readKey', readKey)
    formData.append('files', fs.readFileSync(filePath, 'utf-8'), {
      contentType: 'text/markdown; charset=UTF-8',
      filename: fileName
    })

    const res = await axios.post(url.href, formData, {
      headers: formData.getHeaders()
    })

    t.equal(res.status, 200, 'http statusCode 200')
    t.notEqual(res.data, null, 'http body not null')
    t.notEqual(res.data, undefined, 'http body not undefined')
    t.equal(res.data.driveAddress, driveAddress, `driveAddress is ${driveAddress}`)
    t.equal(res.data.blobId, fileName, `blobId is ${fileName}`)
    t.equal(res.data.readKey, readKey, `readkey is ${readKey}`)
    t.equal(res.data.fileName, fileName, `file name is ${fileName}`)

    const expectedUrl = `http://${server.host}:${server.port}/drive/${driveAddress}/blob/${fileName}?readKey=${readKey}&fileName=${fileName}`
    t.equal(res.data.href, expectedUrl, `href is ${expectedUrl}`)
  }

  server.close()
  t.end()
})
