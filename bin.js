#!/usr/bin/env node
const artefactServer = require('.')
const options = require('minimist')(process.argv.slice(2))

if (options.help) {
  console.log(`Command line options:
--help    display usage information
--host <hostname>  set hostname (defaults to localhost)
--port <port>      set port number (defaults to 1234)
--storage <path>   set local storage directory (defaults to ~/.artefact-store)
--dev              sets storage to /tmp/arefact-store-{Date.now()}
--pataka           be a pataka
  `)
  process.exit(0)
}

artefactServer(options)
